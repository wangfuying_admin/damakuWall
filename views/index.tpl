<!DOCTYPE html>

<html>
<head>
  <title>弹幕墙</title>
  <meta name="renderer" content="webkit" >
  <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
  <meta name="viewport" content="initial-scale=1, user-scalable=0, minimal-ui">
  <script src="/static/js/nicEdit.js"></script>
  <script>
	var container;
	// alert(container)
	var ws = new WebSocket('ws://' + window.location.host + '/ws');
	// var ws = new WebSocket('ws://64.137.190.208:8087/ws');
    ws.onmessage = function(e) {
        // alert(e.data);
		renderDM(e.data);
    };
	ws.onopen = function(e) {
		// alert(ws);
		// ws.send('123s');
	};
	ws.onclose = function(e) {
		document.write("弹幕服务关闭，尝试刷新页面");
	};
	
	window.onunload = function() {
		ws.close();
	};
	
	function sendDM() {
		/*var node = document.getElementById('content');
		var str = node.value;
		node.value = '';
		ws.send(str);*/
		// var area = document.getElementById('area2');
		ws.send(ins.nicInstances[0].getContent());
		// ws.send('Hello');
		ins.nicInstances[0].setContent('');
	}
	
	function sendDMx7() {
		/*var node = document.getElementById('content');
		var str = node.value;
		node.value = '';
		ws.send(str);*/
		// var area = document.getElementById('area2');
		for (var i = 0; i < 7; i ++) {
			ws.send(ins.nicInstances[0].getContent());
		}
		ins.nicInstances[0].setContent('');
	}
	
	function renderDM(content) {
		// alert(content);
		new DM(content);
	}
	
	var dmList = new Array();
	
	// div dan mu block
	function DM(content) {
			this.content = content;
			this.x = container.offsetWidth;
			this.y = container.offsetHeight * 0.8 * Math.random();
			this.div = document.createElement("div");
			this.div.innerHTML='<nobr><font color=black>'+ this.content + '</font></nobr>';
			this.div.style.position = 'absolute';
			// this.div.style.wordWrap = 'keep-all';
			this.div.style.left = this.x + 'px';
			this.div.style.top = (20 + Math.floor(this.y / 20) * 20) + 'px';
			container.appendChild(this.div);
			this.destroy = function() {
				container.removeChild(this.div);
			}
			dmList.push(this);
	}
	
	setInterval(function() {
				for (var i = 0; i < dmList.length; i++) {
					// alert(dmList[i]);
					dmList[i].x -= 1 * (dmList[i].y / 300.0) + 1;
					
					if(dmList[i].x <= -dmList[i].div.offsetWidth * 1.2) {
						dmList[i].destroy();
						dmList.splice(i, 1);
						continue;
					}
					dmList[i].div.style.left = dmList[i].x + 'px';
					
				}
			}, 10);
	
	var nic;
	var ins;
	function init() {
		nic = new nicEditor({iconsPath : '/static/js/nicEditorIcons.gif'});
		ins = nic.panelInstance('area2');
		
		container = document.getElementById('container');
		// alert(container)
	}
	
		
  </script>
  <style>
	* {
		margin: 0px 0px 0px 0px;
		padding: 0px 0px 0px 0px;
		border: none;
	}
  </style>
</head>

<body style="height: 100%; overflow:hidden; background-color:#FFFFFF;" onLoad="init();">
	<center><span>第 {{.Online}} 名</span></center>
	<div id="container" style="position:absolute; top:20px; bottom: 150px; width: 100%;">
	</div>
	<div style="position:absolute; bottom:0px;width:100%;">
		
    	<!--<input type=text id="content" placeholder="输入弹幕内容" onKeyPress="if(event.keyCode==13){sendDM();return false;}" />
        
		<hr />
		<h4>Second Textarea</h4>-->
		<textarea id="area2" name="area2" style="width: 100%;" placeholder="请输入弹幕内容" style="">
			
		</textarea>
		<button type=button onClick="javascript:sendDMx7()" style="width:50%;background-color:#CC0000;color:white;height:54px;display:inline-block;">7连击</button><button type=button onClick="javascript:sendDM()" style="width:50%;background-color:#0C4C8A;color:white;height:54px;display:inline-block;">发送</button>
    </div>
</body>
</html>
